dataSource {
    pooled = true
    dbCreate = ""
    url = "jdbc:mysql://localhost:3306/bookmarks"
    driverClassName = "com.mysql.jdbc.Driver"
    dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
    username = "root"
    password = "root"
}

// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update" // one of 'create', 'create-drop','update'
            url = "jdbc:mysql://localhost:3306/bookmarks"
        }
    }
    test {
        dataSource {
            dbCreate = ""
            url = "jdbc:mysql://localhost:3306/bookmarks"
            //url = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=dpohai.pohai.org.tw)(PORT=8521))(CONNECT_DATA=(SERVICE_NAME=dpohai)))"
        }
    }
    production {
        dataSource {
            dbCreate = ""
            url = "jdbc:mysql://localhost:3306/bookmarks"
        }
    }
}