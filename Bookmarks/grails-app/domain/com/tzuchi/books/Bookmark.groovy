package com.tzuchi.books

class Bookmark {

    static hasMany = [tags:Tag] // addition of hasMany here
    URL url
    String title
    String notes
    Date dateCreated = new Date()

    static constraints = {
    }
}
